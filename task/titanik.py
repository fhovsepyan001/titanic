import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    mr_data = [0, []]
    miss_data = [0, []]
    mrs_data = [0, []]

    for i in range(df.shape[0]):
        row = df.iloc[i]
        if 'Mr.' in row["Name"]:
            if row['Age']:
                mr_data[1] += row["Age"]
            else:
                mr_data[0] += 1

    return None
